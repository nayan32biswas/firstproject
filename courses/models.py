from django.db import models
from django.urls import reverse


class Course(models.Model):
    print("  Courses models Course Created")
    title = models.CharField(max_length=20)

    def get_absolute_url(self):
        #print(" courses models get_absulate_url")
        return reverse("courses:courses-detail", kwargs={"id": self.id})

from django.shortcuts import render, get_object_or_404, redirect
from django.views import View

from .models import Course
from .forms import CourseModelForm


class CourseObjectMixin(object):
    print("  Course Views CourseObjectMixin Created")
    model = Course

    def get_object(self):
        print("   Course   Views CourseObjectMixin----get_object")
        id = self.kwargs.get("id")
        obj = None
        if id is not None:
            obj = get_object_or_404(self.model, id=id)
            print(f"   Course   Views CourseObjectMixin----get_object True")
        print("   Course   Views CourseObjectMixin----get_object return")
        return obj


class CourseDeleteView(CourseObjectMixin, View):
    print("  Course Views CourseDeleteView Created")
    template_name = "courses/course_delete.html"

    def post(self, request, id=None, *args, **kwargs):
        print("   Course   Views CourseDeleteView----post")
        context = {}
        obj = self.get_object()

        if obj is not None:
            obj.delete()
            context["object"] = None
            print(
                "   Course   Views CourseDeleteView----post----True----redirect----delete")
            return redirect("/courses/")
        print("   Course   Views CourseDeleteView----post return")
        return render(request, self.template_name, context)


class CourseUpdateView(CourseObjectMixin, View):
    print("  Course Views CourseUpdateView Created")

    template_name = "courses/course_update.html"

    def get(self, request, id=None, *args, **kwargs):
        print("   Course   Views CourseUpdateView----get")
        context = {}
        obj = self.get_object()
        if obj is not None:
            form = CourseModelForm(instance=obj)
            context["object"] = obj
            context["form"] = form
            print("   Course   Views CourseUpdateView----get----True")
        print("   Course   Views CourseUpdateView----get return")
        return render(request, self.template_name, context)

    def post(self, request, id=None, *args, **kwargs):
        print("   Course   Views CourseUpdateView----post")
        context = {}
        obj = self.get_object()
        if obj is not None:
            print("   Course   Views CourseUpdateView----post True")
            form = CourseModelForm(request.POST, instance=obj)
            if form.is_valid():
                print("   Course   Views CourseUpdateView----post----double----True----save")
                form.save()
            context["object"] = obj
            context["form"] = form
        print("   Course   Views CourseUpdateView----post return ")
        return render(request, self.template_name, context)


class CourseCreateView(View):
    print("  Course Views CourseCreateView Created")
    template_name = "courses/course_create.html"

    def get(self, request, *args, **kwargs):
        print("   Course   Views CourseCreateView----get")
        form = CourseModelForm()
        context = {"form": form}
        print("   Course   Views CourseCreateView----get return")
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        print("   Course   Views CourseCreateView----post")
        form = CourseModelForm(request.POST)
        if form.is_valid():
            print("Condition start")
            form.save()
            print(
                "   Course   Views CourseCreateView----post----True----redirect----save")
            return redirect("/courses/")
        context = {"form": form}
        print("   Course   Views CourseCreateView----post return")
        return render(request, self.template_name, context)


class CourseListView(View):
    print("  Course Views CourseListView Created")
    template_name = "courses/course_list.html"

    def get(self, request, *args, **kwargs):
        print(f"   Course   Views CourseListView----get")
        context = {"object_list": Course.objects.all()}
        print(f"   Course   Views CourseListView----ge return")
        return render(request, self.template_name, context)


class CourseView(CourseObjectMixin, View):
    print("  Course Views CourseView Created")
    template_name = "courses/course_detail.html"

    def get(self, request, id=None, *args, **kwargs):
        print("   Course   Views CourseView----get")
        context = {'object': self.get_object()}
        print("   Course   Views CourseView----get return")
        return render(request, self.template_name, context)

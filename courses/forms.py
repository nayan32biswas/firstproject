from django import forms

from .models import Course


class CourseModelForm(forms.ModelForm):
    print("  Courses forms CourseModelForm")
    class Meta:
        print("   Courses forms CourseModelForm----Meta----Created")
        model = Course
        fields = ["title"]

    def clean_title(self):
        print("   courses forms clean_title")
        title = self.cleaned_data.get("title")
        if title.lower() == "abc":
            raise forms.ValidationError("This is not valid title")
        return title
  
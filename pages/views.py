from django.shortcuts import render

def home_view(request, *args, **kwargs):
    print("    Pages views home_view")
    obj = {
        "my_text": "this is my text",
        "my_list": range(5,50,7),
        "my_name": "Nayan Biswas"
    }
    context = {
        "object": obj
    }
    print("    Pages views home_view  return context")
    return render(request, "home.html", context)


def about_view(request, *args, **kwargs):
    print("    Pages views about_view and return empty")
    return render(request, "about.html", {})


def contact_view(request, *args, **kwargs):
    print("    Pages views contact_view and return empty")
    return render(request, "contact.html", {})

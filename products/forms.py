from django import forms

from .models import Product

# First way


class ProductForm(forms.ModelForm):
    print("  Products forms ProductForm Created")
    title = forms.CharField(
        label="empty",
        widget=forms.TextInput(
            attrs={
                "placeholder": "Product title"
            }
        )
    )
    description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "placeholder": "ProductForm description",
                "class": "new-class-name two",
                "id": "my-id-for-textarea",
                "rows": 10,
                "cols": 50
            }
        )
    )
    price = forms.DecimalField(initial=9.90)

    class Meta:
        print("  Products forms ProductForm----Meta----Created")
        model = Product
        fields = ["title", "description", "price"]

    def clean_title(self, *args, **kwargs):
        print("  Products forms ProductForm---clean_title")
        title = self.cleaned_data.get("title")
        print("  Products forms ProductForm---clean_title return")
        return title

# Second way


class RawProductForm(forms.Form):
    print("  Products forms RawProductForm Created")
    title = forms.CharField(
        label="empty",
        widget=forms.TextInput(
            attrs={
                "placeholder": "RawProductForm title"
            }
        )
    )
    description = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "placeholder": "RawProductForm description",
                "class": "new-class-name two",
                "id": "my-id-for-textarea",
                "rows": 10,
                "cols": 50
            }
        )
    )
    price = forms.DecimalField(initial=199.90)


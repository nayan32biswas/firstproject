from django.shortcuts import render, get_object_or_404, redirect
from .models import Product

from .forms import ProductForm, RawProductForm

def product_create_view(request):
    print("    Products views product_create_view")
    form = ProductForm(request.POST or None)
    if form.is_valid():
        print("    Products views product_create_view")
        form.save()
        #form = ProductForm()

    context = {
        "form": form
    }
    print("    Products views product_create_view----return")
    return render(request, "products/product_create.html", context)

def product_update_view(request, id):
    print("    Products views product_update_view")
    obj = get_object_or_404(Product, id=id)
    form = ProductForm(request.POST or None, instance=obj)
    if form.is_valid():
        print("    Products views product_update_view True")
        form.save()
        form = ProductForm()

    context = {
        "form": form
    }
    print("    Products views product_update_view----return")
    return render(request, "products/product_create.html", context)

def product_list_view(request):
    print("    Products views product_list_view")
    queryset = Product.objects.all()
    context = {
        "object_list": queryset
    }
    print("    Products views product_list_view----return")
    return render(request, "products/product_list.html", context)

def product_detail_view(request, id):
    print("    Products views product_detail_view")
    obj = get_object_or_404(Product, id=id)
    context = {
        "object": obj
    }
    print("    Products views product_detail_view----return")
    return render(request, "products/product_detail.html", context)

def product_delete_view(request, id):
    print("    Products views product_delete_view")
    obj = get_object_or_404(Product, id=id)
    if request.method == "GET":
        print("    Products views product_delete_view--------redirect")
        obj.delete()
        return redirect("../../")
    context = {
        "object": obj
    }
    print("    Products views product_delete_view----return")
    return render(request, "products/product_list.html", context)


from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView
)
from .forms import ArticleModelForm
from .models import Article


class ArticleCreateView(CreateView):
    print("  Blog views ArticleCreateView Created and extend CreateView")
    template_name = "article/article_create.html"
    form_class = ArticleModelForm

    def form_valid(self, form):
        print("    Blog views ArticleCreateView----form_valid and return")
        return super().form_valid(form)


class ArticleListView(ListView):
    print("  Blog views ArticleListView Created and extend ListView")
    template_name = "article/article_list.html"
    queryset = Article.objects.all()


class ArticleDetailView(DetailView):
    print("  Blog views ArticleDetailView Created and extend DetailView")
    template_name = "article/article_detail.html"

    def get_object(self):
        print("    Blog views ArticleDetailView----get_object")
        id_ = self.kwargs.get("id")
        print("    Blog views ArticleDetailView----get_object return")
        return get_object_or_404(Article, id=id_)


class ArticleUpdateView(UpdateView):
    print("  Blog views ArticleUpdateView Created and extend UpdateView")
    template_name = "article/article_create.html"
    form_class = ArticleModelForm

    def get_object(self):
        print("    Blog views ArticleUpdateView----get_object")
        id_ = self.kwargs.get("id")
        print("    Blog views ArticleUpdateView----get_object return")
        return get_object_or_404(Article, id=id_)

    def form_valid(self, form):
        print("    Blog views ArticleUpdateView----form_valid and return")
        return super().form_valid(form)

class ArticleDeleteView(DeleteView):
    print("  Blog views ArticleDeleteView Created and extend DeleteView")
    template_name = "article/article_delete.html"

    def get_object(self):
        print("    Blog views ArticleDeleteView----get_object")
        id_ = self.kwargs.get("id")
        print("    Blog views ArticleDeleteView----get_object return")
        return get_object_or_404(Article, id=id_)
    
    def get_success_url(self):
        print("    Blog views ArticleDeleteView----get_success_url and return")
        return reverse("articles:article-list")

from django.db import models
from django.urls import reverse

class Article(models.Model):
    print("  Blog models Article Created")
    title       = models.CharField(max_length=120)
    description = models.TextField(blank=True, null=True)
    summary     = models.TextField(blank=False, null=False)

    def get_absolute_url(self):
        return reverse("articles:article-detail", kwargs={"id": self.id})

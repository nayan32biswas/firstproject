from django import forms

from .models import Article


class ArticleModelForm(forms.ModelForm):
    print("  Blog forms ArticleModelForm Created")
    class Meta:
        print("   Blog forms ArticleModelForm----Meta Created")
        model = Article
        fields = ["title","description"]

    def clean_title(self, *args, **kwargs):
        print("    Blog forms ArticleModelForm clean_title")
        title = self.cleaned_data.get("title")
        return title